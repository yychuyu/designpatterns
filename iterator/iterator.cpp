#include "iterator.h"

MyIterator::MyIterator(Aggregate *ag)
{
	m_ag = ag;
	m_index = 0;
}

void MyIterator::first()
{
	m_index = 0;
}

void MyIterator::next()
{
	if (m_index < m_ag->getSize())
		m_index++;
}

bool MyIterator::isDone()
{
	return (m_index == m_ag->getSize());
}

Object MyIterator::currentItem()
{
	return m_ag->getItem(m_index);
}
