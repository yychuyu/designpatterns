#ifndef ITERATOR_H_
#define ITERATOR_H_
#include "aggregate.h"

typedef int Object;
class Aggregate;
class Iterator
{
public:
	virtual void first() = 0;
	virtual void next() = 0;
	virtual bool isDone() = 0;
	virtual Object currentItem() = 0;
};

class MyIterator: public Iterator
{
public:
	MyIterator(Aggregate *ag);
	void first();
	void next();
	bool isDone();
	Object currentItem();
	
private:
	int m_index;
	Aggregate *m_ag;
};

#endif