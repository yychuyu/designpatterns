#include "aggregate.h"

ConcreteAggregate::ConcreteAggregate()
{
	for (int i = 0; i < SIZE; i++)
		obj[i] = i + 100;
}

Iterator *ConcreteAggregate::createIterator()
{
	return new MyIterator(this);
}

Object ConcreteAggregate::getItem(int index)
{
	return obj[index];
}

int ConcreteAggregate::getSize()
{
	return SIZE;
}
