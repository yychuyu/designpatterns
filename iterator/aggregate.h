#ifndef AGGREGATE_H_
#define AGGREGATE_H_

#include "iterator.h"
#include <iostream>
using namespace std;
typedef int Object;
#define SIZE 5

class Iterator;
class Aggregate
{
public:
	virtual Iterator *createIterator() = 0;
	virtual Object getItem(int index) = 0;
	virtual int getSize() = 0;
};

class ConcreteAggregate: public Aggregate
{
public:
	ConcreteAggregate();
	Iterator *createIterator();
	Object getItem(int index);
	int getSize();
	
private:
	Object obj[SIZE];
};

#endif