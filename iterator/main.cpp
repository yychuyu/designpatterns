#include "aggregate.h"
#include "iterator.h"
#include <iostream>
using namespace std;

int main()
{
	Aggregate *ag = new ConcreteAggregate;
	Iterator *it = ag->createIterator();
	for (; !(it->isDone()); it->next())
		cout << it->currentItem() << " ";
	cout << endl;
	delete it;
	delete ag;
	return 0;
}